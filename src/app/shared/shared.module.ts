import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalModule } from '../global/global.module';

// import material components
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { RouterModule } from '@angular/router'; 

import { FooterComponent } from './components/footer/footer.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { NavigatorComponent } from './components/navigator/navigator.component';
import { MenuProfileComponent } from './components/menu-profile/menu-profile.component';
import { WindowLoaderComponent } from './components/window-loader/window-loader.component';
import { WindowMessageComponent } from './components/window-message/window-message.component';
import { ReaderImageComponent } from './components/reader-image/reader-image.component';

@NgModule({
  declarations: [
    FooterComponent,
    ToolbarComponent,
    NavigatorComponent,
    MenuProfileComponent,
    WindowLoaderComponent,
    WindowMessageComponent,
    ReaderImageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    GlobalModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
  ],
  exports: [
    FooterComponent,
    ToolbarComponent,
    NavigatorComponent,
    MenuProfileComponent,
    WindowLoaderComponent,
    WindowMessageComponent,
    ReaderImageComponent
  ]
})
export class SharedModule { }
