import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WindowMessageComponent } from './window-message.component';

describe('WindowMessageComponent', () => {
  let component: WindowMessageComponent;
  let fixture: ComponentFixture<WindowMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WindowMessageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WindowMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
