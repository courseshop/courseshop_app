import { Component, Input } from '@angular/core';
import { MenuRoutesDto } from 'src/app/global/Dtos/menu-routes-dto';

@Component({
  selector: 'app-menu-profile',
  templateUrl: './menu-profile.component.html',
  styleUrls: ['./menu-profile.component.scss']
})
export class MenuProfileComponent {
  @Input() dataRoutes: Array<MenuRoutesDto> = []
  constructor() {}
}
