import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaderImageComponent } from './reader-image.component';

describe('ReaderImageComponent', () => {
  let component: ReaderImageComponent;
  let fixture: ComponentFixture<ReaderImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaderImageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReaderImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
