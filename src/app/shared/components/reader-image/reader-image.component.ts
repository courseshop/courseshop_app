import { Component, OnChanges, SimpleChanges, OnInit, ViewChild, ElementRef, Renderer2, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-reader-image',
  templateUrl: './reader-image.component.html',
  styleUrls: ['./reader-image.component.scss']
})
export class ReaderImageComponent  implements OnChanges, OnInit{
  @ViewChild('file', { static: true }) file: ElementRef = (<any> window);
  @ViewChild('image', { static: true }) image: ElementRef = (<any> window);
  @Output() onData: EventEmitter<File> = new EventEmitter();
  
  name: string ="";
  files: FileList = (<any> window);
  filestring: string = "";
  defaultSrc: String = "https://cdn1.iconfinder.com/data/icons/web-seo-and-marketing/512/camera-512.png";
  constructor(
    private _render: Renderer2,
    private _ChangeDetectorRef:ChangeDetectorRef
  ) {}
  ngOnInit(): void {}
  ngOnChanges(changes: SimpleChanges): void { }
  // Custom Fuctions
  onUploadImage() { this.file.nativeElement?.click() };
  load(event:any) {
    try {
      this.files = event.target.files;
      if (this.files.length < 1) {
        throw new Error('No ha seleccionado alguna imagen del directorio.');
      } else if (this.files.length > 1) {
        throw new Error('Ha seleccionado mas de una imagen del directorio.');
      }
      this.name = this.files[0].name;
      this.onData.emit(this.files[0]);
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(this.files[0]);
    } catch (error) {
      console.log("error: ", error)
    }
  }

  private _handleReaderLoaded(readerEvt: any) {
    this._render.setAttribute(this.image.nativeElement, 'src', readerEvt.target.result);
  }
}
