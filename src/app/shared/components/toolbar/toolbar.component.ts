import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {
  burgerVisible: Boolean = true;
  menuProfileVisible: Boolean = false;
  @Input() profile= {};
  @Output() onClickBurger = new EventEmitter();
  @Output() onClickAvatar = new EventEmitter();
  constructor() {

  }
  eventClickBurger(e: any) {
    this.burgerVisible = !this.burgerVisible;
    this.onClickBurger.emit({visible: this.burgerVisible})
  };
  eventClickAvatar(e: any) {
    this.menuProfileVisible = !this.menuProfileVisible;
    this.onClickAvatar.emit({visible: this.menuProfileVisible})
  }
}
