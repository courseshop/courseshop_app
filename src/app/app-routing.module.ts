import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component'

const routes: Routes = [
  { path: "auth", loadChildren: () => import("./modules/authentication/authentication.module").then(mo => mo.AuthenticationModule) },
  { path: "", loadChildren: () => import("./modules/courses/courses.module").then(mo => mo.CoursesModule) },
  { path: "perfil", loadChildren: () => import("./modules/profile/profile.module").then(mo => mo.ProfileModule) },
  { path: "usuarios", loadChildren: () => import("./modules/users/users.module").then(mo => mo.UsersModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
