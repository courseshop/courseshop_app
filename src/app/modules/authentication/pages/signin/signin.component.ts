import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent {
  signinForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });
  constructor() {}
  login(e: any) {
    if(this.signinForm.get("email")?.dirty && this.signinForm.get("password")?.dirty) {
      console.log(this.signinForm.get("email")?.value)
      console.log(this.signinForm.get("password")?.value)
    } else {
      console.log("campos incorrectos")
    }
  }
}
