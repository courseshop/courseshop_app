import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit{
  ImgAvatar: File = new File([], "");
  signupForm = new FormGroup({
    name: new FormControl(''),
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    dni: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
  });
  constructor(private formBuilder: FormBuilder) {}
  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      name: ["",[Validators.required ]],
      firstname: ["",[Validators.required ]],
      lastname: ["",[Validators.required ]],
      dni: ["",[Validators.required ]],
      email: ["",[Validators.required, Validators.email]],
      password: ["",[Validators.required]],
    })
  }

  // Custom Functions
  signup(e: any) {
    try {
      if(this.signupForm.invalid || !this.ImgAvatar?.name || this.ImgAvatar?.name === "" || this.ImgAvatar?.name === null) {
        throw new Error(`No se completaron los campos del formulario`)
      }
      const formData = new FormData();
      formData.append("name",`${this.signupForm.get("name")?.value}`)
      formData.append("firstname",`${this.signupForm.get("firstname")?.value}`)
      formData.append("lastname",`${this.signupForm.get("lastname")?.value}`)
      formData.append("dni",`${this.signupForm.get("dni")?.value}`)
      formData.append("email",`${this.signupForm.get("email")?.value}`)
      formData.append("password",`${this.signupForm.get("password")?.value}`)
      formData.append("avatar", this.ImgAvatar, this.ImgAvatar?.name);

    } catch (error) {
      console.log("error: ", error)
    }
  }
  loadImage(event: File) { this.ImgAvatar = event; }
}
