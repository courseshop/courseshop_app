import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import angular material
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router'; 

import { SharedModule } from '../../shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { UserHomePageComponent } from './pages/user-home-page/user-home-page.component';


@NgModule({
  declarations: [
    UserHomePageComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MatRippleModule,
    SharedModule,
    MatButtonModule,
    MatIconModule,
    RouterModule,
  ],
  exports: [
    UserHomePageComponent
  ]
})
export class UsersModule { }
