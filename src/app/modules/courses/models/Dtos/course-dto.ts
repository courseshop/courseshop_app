export class CourseDto{
    name: String = "";
    summary: String = "";
    picture: String = "";
    autor: String = "";
    rating: Number = 0;
}