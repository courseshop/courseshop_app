export interface ICourseEntity {
    name: String;
    summary: String;
    picture: String;
    backgroundPicture: String;
    autor: String;
    rating: Number;
}
export class CourseEntity implements ICourseEntity{
    name: String = "";
    summary: String = "";
    picture: String = "";
    backgroundPicture: String = "";
    autor: String = "";
    rating: Number = 0;
}
