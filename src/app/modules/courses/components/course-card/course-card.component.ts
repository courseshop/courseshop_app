import { Component, Input } from '@angular/core';
import { CourseEntity } from '../../models/Entities/course-entity';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})
export class CourseCardComponent {
  @Input() course = new CourseEntity();
  constructor() {}
}
