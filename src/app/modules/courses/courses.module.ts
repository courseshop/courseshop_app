import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


// import angular material
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router'; 

// Complements
import { SharedModule } from '../../shared/shared.module';
import { CoursesRoutingModule } from './courses-routing.module';

// Custom Components
import { CourseCardComponent } from './components/course-card/course-card.component';

// Pages
import { CoursePortafolioComponent } from './pages/process/course-portafolio/course-portafolio.component';
import { ManageCoursesComponent } from './pages/maintenance/manage-courses/manage-courses.component';




@NgModule({
  declarations: [
    CoursePortafolioComponent,
    CourseCardComponent,
    ManageCoursesComponent
  ],
  imports: [
    CommonModule,
    CoursesRoutingModule,
    SharedModule,
    MatRippleModule,
    MatButtonModule,
    MatIconModule,
    RouterModule,
  ],
  exports: [
    CoursePortafolioComponent,
    CourseCardComponent,
    ManageCoursesComponent
  ]
})
export class CoursesModule { }
