import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursePortafolioComponent } from './course-portafolio.component';

describe('CoursePortafolioComponent', () => {
  let component: CoursePortafolioComponent;
  let fixture: ComponentFixture<CoursePortafolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoursePortafolioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoursePortafolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
