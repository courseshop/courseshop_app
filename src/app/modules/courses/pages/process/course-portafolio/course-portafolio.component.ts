import { Component } from '@angular/core';
import { MenuRoutesDto } from 'src/app/global/Dtos/menu-routes-dto';
import { Constants } from 'src/app/global/constants';

@Component({
  selector: 'app-course-portafolio',
  templateUrl: './course-portafolio.component.html',
  styleUrls: ['./course-portafolio.component.scss']
})
export class CoursePortafolioComponent {
  navigatorVisible: Boolean = true; 
  menuProfileVisible: Boolean = false; 
  dataRoutes: Array<MenuRoutesDto> = new Constants().dataRoutesLogin;
  courses = [
    {
      name: "Curso de Capywriting para Plataformas Digitales",
      summary: "¡Bienvenido! En este curso tu profesora será Diana Reyes (@Tifis) quien tiene experiencia en áreas como copywriting, publicidad, UX, email marketing y social media.",
      picture: "https://w7.pngwing.com/pngs/804/39/png-transparent-man-face-image-file-formats-face-people.png",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Diana Reyes",
      rating: 1,
    },
    {
      name: "Curso de Estrategia y Planeacion de Contenidos para SEO",
      summary: "Durante el Curso de Estrategia y Planeación de Contenidos para SEO vamos a ver todo el proceso de cómo crear una estrategia, incluyendo la planeación, ejecución y el mantenimiento, para llegar a los primeros resultados en Google.",
      picture: "https://p1.pxfuel.com/preview/34/278/425/male-black-and-white-portrait-face-young-model.jpg",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Alejandro Gonzales",
      rating: 1,
    },
    {
      name: "Curso de Buenas Practicas de SEO",
      summary: "En el Curso de Buenas Prácticas de SEO conocerás la importancia de la optimización de tu contenido para los motores de búsqueda, destacando recomendaciones y acciones necesarias para realizar un buen trabajo.",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Natalia Prieto",
      rating: 1,
    },
    {
      name: "Curso de Excel para el Analisis de Datos",
      summary: "-",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Felipe Gusman",
      rating: 1,
    },
    {
      name: "Curso de SEO para Ecommerce",
      summary: "-",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Alejandro Gonzalez",
      rating: 1,
    },
    {
      name: "Curso de Capywriting para Plataformas Digitales",
      summary: "¡Bienvenido! En este curso tu profesora será Diana Reyes (@Tifis) quien tiene experiencia en áreas como copywriting, publicidad, UX, email marketing y social media.",
      picture: "https://w7.pngwing.com/pngs/804/39/png-transparent-man-face-image-file-formats-face-people.png",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Diana Reyes",
      rating: 1,
    },
    {
      name: "Curso de Estrategia y Planeacion de Contenidos para SEO",
      summary: "Durante el Curso de Estrategia y Planeación de Contenidos para SEO vamos a ver todo el proceso de cómo crear una estrategia, incluyendo la planeación, ejecución y el mantenimiento, para llegar a los primeros resultados en Google.",
      picture: "https://p1.pxfuel.com/preview/34/278/425/male-black-and-white-portrait-face-young-model.jpg",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Alejandro Gonzales",
      rating: 1,
    },
    {
      name: "Curso de Buenas Practicas de SEO",
      summary: "En el Curso de Buenas Prácticas de SEO conocerás la importancia de la optimización de tu contenido para los motores de búsqueda, destacando recomendaciones y acciones necesarias para realizar un buen trabajo.",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Natalia Prieto",
      rating: 1,
    },
    {
      name: "Curso de Excel para el Analisis de Datos",
      summary: "-",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Felipe Gusman",
      rating: 1,
    },
    {
      name: "Curso de SEO para Ecommerce",
      summary: "-",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Alejandro Gonzalez",
      rating: 1,
    },
    {
      name: "Curso de Capywriting para Plataformas Digitales",
      summary: "¡Bienvenido! En este curso tu profesora será Diana Reyes (@Tifis) quien tiene experiencia en áreas como copywriting, publicidad, UX, email marketing y social media.",
      picture: "https://w7.pngwing.com/pngs/804/39/png-transparent-man-face-image-file-formats-face-people.png",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Diana Reyes",
      rating: 1,
    },
    {
      name: "Curso de Estrategia y Planeacion de Contenidos para SEO",
      summary: "Durante el Curso de Estrategia y Planeación de Contenidos para SEO vamos a ver todo el proceso de cómo crear una estrategia, incluyendo la planeación, ejecución y el mantenimiento, para llegar a los primeros resultados en Google.",
      picture: "https://p1.pxfuel.com/preview/34/278/425/male-black-and-white-portrait-face-young-model.jpg",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Alejandro Gonzales",
      rating: 1,
    },
    {
      name: "Curso de Buenas Practicas de SEO",
      summary: "En el Curso de Buenas Prácticas de SEO conocerás la importancia de la optimización de tu contenido para los motores de búsqueda, destacando recomendaciones y acciones necesarias para realizar un buen trabajo.",
      picture: "https://media.istockphoto.com/id/537252582/es/foto/mujer-joven-sonriendo-y-confianza-vertical.jpg?s=612x612&w=0&k=20&c=ykzmfaLDRfj3IsSC9VIklXFL2MEv8hEDj0t64KdK2GM=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Natalia Prieto",
      rating: 1,
    },
    {
      name: "Curso de Excel para el Analisis de Datos",
      summary: "-",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Felipe Gusman",
      rating: 1,
    },
    {
      name: "Curso de SEO para Ecommerce",
      summary: "-",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Alejandro Gonzalez",
      rating: 1,
    },
    {
      name: "Curso de Capywriting para Plataformas Digitales",
      summary: "¡Bienvenido! En este curso tu profesora será Diana Reyes (@Tifis) quien tiene experiencia en áreas como copywriting, publicidad, UX, email marketing y social media.",
      picture: "https://media.istockphoto.com/id/537252582/es/foto/mujer-joven-sonriendo-y-confianza-vertical.jpg?s=612x612&w=0&k=20&c=ykzmfaLDRfj3IsSC9VIklXFL2MEv8hEDj0t64KdK2GM=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Diana Reyes",
      rating: 1,
    },
    {
      name: "Curso de Estrategia y Planeacion de Contenidos para SEO",
      summary: "Durante el Curso de Estrategia y Planeación de Contenidos para SEO vamos a ver todo el proceso de cómo crear una estrategia, incluyendo la planeación, ejecución y el mantenimiento, para llegar a los primeros resultados en Google.",
      picture: "https://p1.pxfuel.com/preview/34/278/425/male-black-and-white-portrait-face-young-model.jpg",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Alejandro Gonzales",
      rating: 1,
    },
    {
      name: "Curso de Buenas Practicas de SEO",
      summary: "En el Curso de Buenas Prácticas de SEO conocerás la importancia de la optimización de tu contenido para los motores de búsqueda, destacando recomendaciones y acciones necesarias para realizar un buen trabajo.",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Natalia Prieto",
      rating: 1,
    },
    {
      name: "Curso de Excel para el Analisis de Datos",
      summary: "-",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Felipe Gusman",
      rating: 1,
    },
    {
      name: "Curso de SEO para Ecommerce",
      summary: "-",
      picture: "https://media.istockphoto.com/id/656673020/es/foto/guapo-en-gafas.jpg?s=612x612&w=is&k=20&c=1GBzWCp5ICdA2zQ2WdbDcC7S8AuQ-lXeNJ4Mb2cVkek=",
      backgroundPicture: "https://thumbs.dreamstime.com/z/credit-card-mobile-payment-online-shopping-digital-marketing-concept-social-media-communication-networking-icons-modern-152127680.jpg",
      autor: "Alejandro Gonzalez",
      rating: 1,
    },
  ]
  constructor() {
  }
  onBurger({visible}: { visible: Boolean }) {
    this.navigatorVisible = visible;
  }
  onAvatar({visible}: { visible: Boolean }) {
    this.menuProfileVisible = visible;
  }
}
