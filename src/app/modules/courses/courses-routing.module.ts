import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoursePortafolioComponent } from './pages/process/course-portafolio/course-portafolio.component';

const routes: Routes = [
  {
    path: "", component: CoursePortafolioComponent
  },
  {
    path: "cursos", component: CoursePortafolioComponent
  },
  // {
  //   path: '**', redirectTo: '/auth/login'
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
