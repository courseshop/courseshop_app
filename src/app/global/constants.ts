import { MenuRoutesDto } from './Dtos/menu-routes-dto';
export class Constants {
    dataRoutesLogin: Array<MenuRoutesDto>=[
        {
          name: "Iniciar Sesion",
          tooltip: "Iniciar Sesion",
          icon: "",
          route: "/auth/login",
          children: []
        },
        {
          name: "Registro",
          tooltip: "Registrarse",
          icon: "",
          route: "/auth/registro",
          children: []
        }
      ]
    dataRoutesProfile: Array<MenuRoutesDto>=[
      {
        name: "Mi Cuenta",
        tooltip: "Mi Cuenta",
        icon: "",
        route: "/perfil",
        children: []
      },
      {
        name: "Cerrar Sesion",
        tooltip: "Cerrar Sesion",
        icon: "",
        route: "",
        children: []
      }
    ]

}
