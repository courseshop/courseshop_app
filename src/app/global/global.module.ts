import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Constants } from './constants';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    
  ],
  providers:[
    Constants
  ]
})
export class GlobalModule { }
