export class MenuRoutesDto {
    name: String = "";
    tooltip: String = "";
    icon: String = "";
    route: String = ""; 
    children: Array<MenuRoutesDto> = [] 
}
